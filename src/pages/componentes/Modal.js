import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import "../../css/app.css";



class Modal extends React.Component {

    state = {
        value: '',
        emails: [],
        contador: 0,
    }
    handleChange = (evt) => {
        this.setState({
            value: evt.target.value
        });
    };
    handleKeyDown = evt => {
        if (['Enter', 'Tab', ','].includes(evt.key)) {
            evt.preventDefault();

            var email = this.state.value.trim();

            if (email) {
                this.setState({
                    emails: [...this.state.emails, email],
                    value: ''
                });
            }
        }
    };
    handleDelete = (toBeRemoved) => {
        this.setState({
            emails: this.state.emails.filter(email => email !== toBeRemoved)
        });
    };
    render() {
        if (!this.props.show) {
            return null;
        }
        const backStyle = {
            position: 'fixed',
            top: 30,
            bottom: 0,
            left: 'auto',
            right: 'auto',
            backgroundColor: 'rgba(100,100,100,0.7)',
            padding: 50,
            marginLeft: -44,
            width:'100%',
        };
        const modalStyle = {
            backgroundColor: '#f1f1f1',
            borderRadius: 5,
            width: 600,
            heigth: 'auto',
            margin: 'auto',
            padding: 30, 
        };
        const btnStyle = {
            backgroundColor: '#f44336',
            color: '#fff',
            marginTop: 10,
        };
        return (
            <main justify="center" className="wrapper">
                <div className="back" style={backStyle}>
                    <div className="modal" style={modalStyle}>
                        <h1 style={{ textAlign: 'center', }}>Participantes</h1>
                        <hr />
                        {this.state.emails.map(email => (
                            <React.Fragment>
                                <div className="tag-item" key={email}>{email}
                                    <button type="button" className="button" onClick={() => this.handleDelete(email)}>&times;</button>
                                </div>
                            </React.Fragment>
                        ))}
                        {this.props.children}
                        <TextField style={{ marginTop: "auto" }}
                            fullWidth label="Escreva ou cole o endereço de e-mail, e pressione 'Enter'... " variant="filled"
                            value={this.state.value}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDown}
                            className={"input " + (this.state.error && " has-error")}
                        />
                        {this.state.error && <p className="error">{this.state.error}</p>}
                        <div className="footer">
                            <Button style={btnStyle} onClick={this.props.onClose}>
                                Close
                      </Button>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    children: PropTypes.node
};

export default Modal; 