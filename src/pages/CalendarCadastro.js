import React, { Component, Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import { connect } from 'react-redux';
import Modal from './componentes/Modal';



class CalendarCadastro extends Component {  
  constructor(props) {
    super(props);

    this.state = { 
      isOpen: false,
      value: '',
      emails: [], 
    };
  }

  listEmails(){
    this.setState({
      emails: [this.state.emails]
    })
  }

  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  send = () => {
    console.log(this.state.value);
  }

  render() {
    return (
      <Fragment>
        <FormControl fullWidth>
          <Grid container justify="center" alignItems="center" xs={12}>
            <h1 style={{ fontSize: 45, color: '#212121', }}>Cadastro</h1>
          </Grid>
          <Grid container jusfify="space-aroud" spacing={6}>
            <Grid item xs={6}>
              <TextField margin="normal" fullWidth label="Nome" />
            </Grid>
            <Grid item xs={6}>
              <TextField margin="normal" fullWidth label="E-mail" />
            </Grid>
          </Grid>
          <Grid container justify="center" xs={12}>
            <Grid item xs={4}>
              <TextField style={{ marginTop: 20, marginBottom: 20, }} fullWidth type="datetime-local" />
            </Grid>
          </Grid>
          <Grid container xs={12}>
          <Grid item xs={12}>
              {this.state.emails.map(email=>(
                <React.Fragment>
                  <div className="tag-item" key={email}>{email}
                    <button type="button" className="button"></button>
                  </div>
                </React.Fragment>
              ))}
            </Grid>
          </Grid>
          <Grid container justify="center" xs={12}>
            <Grid item xs={12}>
              <TextField label="Descrição" multiline rows={6} fullWidth variant="filled" />
            </Grid>
          </Grid>
        </FormControl>
        <Button style={{ color: '#fff', backgroundColor: '#f44336', marginTop: 30, }}>Cadastrar</Button>
        <Button style={{ color: '#fff', backgroundColor: '#f44336', marginTop: 30, marginLeft: 10, }}
          onClick={this.toggleModal} >+</Button>
        <Modal container justify="center" xs={12} show={this.state.isOpen} onClose={this.toggleModal}>
          
        </Modal>
      </Fragment>
    );
  }
}

const pattern = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);

const mapStateToProps = state => {
  return {
    state: state
  }
}

export default connect(mapStateToProps)(CalendarCadastro);

